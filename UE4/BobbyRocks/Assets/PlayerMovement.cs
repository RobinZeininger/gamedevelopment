﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float runSpeed = 10f;
    public float jumpPower = 100f;
    private bool facingRight;
    public GameObject powerShield;
    public GameObject e = null;

    void Start() {
        facingRight = true;
    }

    void Update() {
        Vector3 position = transform.position;

        if (Input.GetKey(KeyCode.D)) {
            position.x += runSpeed * Time.deltaTime;
            print("D");
        }

        if (Input.GetKey(KeyCode.A))
        {
            position.x -= runSpeed * Time.deltaTime;
            print("A");
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            position.y += jumpPower * Time.deltaTime;
            print("A");
        }

        transform.position = position;

        if (e != null) {
            e.transform.position = transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Shield(Clone)")
        {
            e = Instantiate(powerShield);
            e.transform.position = transform.position;
        }
    }

    void FixedUpdate() {
        float horizontal = Input.GetAxis("Horizontal");
        Flip(horizontal);
    }

    private void Flip(float horizontal) {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
            facingRight = !facingRight;
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
    }
}
