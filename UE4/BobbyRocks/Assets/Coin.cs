﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Bobby" || other.gameObject.name == "PowerShield(Clone)")
        {
            Score.scoreValue += 1;
        }

        if (other.gameObject.name == "Ground")
        {
            Destroy(this.gameObject);
        }
    }
}
