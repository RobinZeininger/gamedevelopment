﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject explosion;

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject e = Instantiate(explosion) as GameObject;
        e.transform.position = transform.position;
        if (other.gameObject.name == "Bobby" || other.gameObject.name == "PowerShield(Clone)")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }

        if (other.gameObject.name == "Ground")
        {
            Destroy(this.gameObject);
        }
    }
}
