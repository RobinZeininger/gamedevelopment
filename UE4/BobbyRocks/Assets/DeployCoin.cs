﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployCoin : MonoBehaviour
{
    public GameObject Coin;
    public GameObject Shield;
    public float respawnTime = 2.0f;
    private Vector2 screenBounds;

    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(coinWave());
    }
    private void spawnCoin()
    {
        GameObject a = Instantiate(Coin) as GameObject;
        a.transform.position = new Vector2(Random.Range(-screenBounds.x, screenBounds.x * 1.1f), screenBounds.y * 2);

        if (Score.scoreValue == 10 || Score.scoreValue == 15 || Score.scoreValue == 20)
        {
            spawnShield();
        }
    }

    private void spawnShield()
    {
        GameObject b = Instantiate(Shield) as GameObject;
        b.transform.position = new Vector2(Random.Range(-screenBounds.x, screenBounds.x * 1.1f), screenBounds.y * 2);
    }
    IEnumerator coinWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnCoin();
        }
    }
}
