﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployEnemy : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject BigEnemy;
    public float respawnTime = 0.5f;
    private Vector2 screenBounds;

    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(enemyWave());
    }
    private void spawnEnemy()
    {
        GameObject a = Instantiate(Enemy) as GameObject;
        a.transform.position = new Vector2(Random.Range(-screenBounds.x, screenBounds.x * 1.1f), screenBounds.y * 2);
    }

    private void spawnHUGEEnemies()
    {
        GameObject b = Instantiate(BigEnemy) as GameObject;
        b.transform.position = new Vector2(Random.Range(-screenBounds.x, screenBounds.x * 1.1f), screenBounds.y * 2);
    }
    IEnumerator enemyWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnEnemy();
            if (Score.scoreValue > 10) {
                spawnHUGEEnemies();
            }
        }
    }
}