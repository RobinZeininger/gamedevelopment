﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    GameObject prefab;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        prefab = Resources.Load("projectile") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject projectileLeft = Instantiate(prefab) as GameObject;
            Destroy(projectileLeft.gameObject, 3);
            projectileLeft.transform.position = transform.position + player.transform.forward * 2 + player.transform.up * -2 + player.transform.right * -2;
            Rigidbody rbLeft = projectileLeft.GetComponent<Rigidbody>();
            rbLeft.velocity = Camera.main.transform.forward * 400f;

            GameObject projectileRight = Instantiate(prefab) as GameObject;
            Destroy(projectileRight.gameObject, 3);
            projectileRight.transform.position = transform.position + player.transform.forward * 2 + player.transform.up * -2 + player.transform.right * 2;
            Rigidbody rbRight = projectileRight.GetComponent<Rigidbody>();
            rbRight.velocity = Camera.main.transform.forward * 400f;
        }
    }
}
