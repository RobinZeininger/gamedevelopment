﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float runSpeed = 100f;
    public float baseSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            float turnSpeed = runSpeed * Time.deltaTime;
            float rotateLeft = Input.GetAxis("Horizontal") * turnSpeed;
            transform.Rotate(-Vector3.forward * rotateLeft);
            print("A");
        }

        if (Input.GetKey(KeyCode.A))
        {
            float turnSpeed = runSpeed * Time.deltaTime;
            float rotateRight = Input.GetAxis("Horizontal") * turnSpeed;
            transform.Rotate(-Vector3.forward * rotateRight);
            print("D");
        }

        if (Input.GetKey(KeyCode.W))
        {
            float turnSpeed = runSpeed * Time.deltaTime;
            float rotateDown = Input.GetAxis("Vertical") * turnSpeed;
            transform.Rotate(Vector3.right * rotateDown);
        }

        if (Input.GetKey(KeyCode.S))
        {
            float turnSpeed = runSpeed * Time.deltaTime;
            float rotateUp = Input.GetAxis("Vertical") * turnSpeed;
            transform.Rotate(Vector3.right * rotateUp);
        }
        transform.Translate(Vector3.forward * runSpeed * Time.deltaTime);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PowerUpContainerYellow")
        {
            ManaBar.barImage.fillAmount += 0.5f;
        }
    }
}
