﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveProjectile : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Score.scoreValue += 1;
        Destroy(other.gameObject);
        Destroy(this.gameObject);
    }
}
