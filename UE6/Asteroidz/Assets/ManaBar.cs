﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeMonkey;

public class ManaBar : MonoBehaviour {
    public static Image barImage;

    void Awake() {
        barImage = transform.Find("bar").GetComponent<Image>();
        barImage.fillAmount = 1f;
    }

    void Update() {
        barImage.fillAmount = barImage.fillAmount - .0005f;
        if (barImage.fillAmount == 0f) {
            SceneManager.LoadScene("SampleScene");
            Score.scoreValue = 0;
            barImage.fillAmount = 1f;
        }
    }
}